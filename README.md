dds2png
====
Программа для конвертации dds в png

Установка
----
Выполнить pip install -r ./requirements.txt

Использование
---
1. Выполнить python main.py для того, чтобы создать директории input и output.
2. Положить файлы с расширением *.dds в папку Input.
3. Выполнить команду python main.py
4. Забрать готовые файлы из папки output