import os
from PIL import Image


def convert_files():
    for file_name in os.listdir("input"):
        file_path = os.path.join('.\input', file_name)
        file_name, ext = file_name.rsplit('.', 1)
        if os.path.isfile(file_path) and ext.lower() == 'dds':
            with Image.open(file_path) as image:
                print(file_path)
                g, b, a, r = image.split()
                png = Image.merge('RGB', (r, g, b))
                png.save(f'.\output\\{file_name}.png')


def init_dirs():
    os.makedirs('./input', exist_ok=True)
    os.makedirs('./output', exist_ok=True)


if __name__ == '__main__':
    init_dirs()
    convert_files()
